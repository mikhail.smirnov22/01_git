package com.epam;

/**
 * Hello world!
 *
 */
public class App 
{
    private static final int NUMBERS = 10;

    public static void main( String[] args )
    {
        for (int i = 0; i < NUMBERS; i++) {
            if(i==5) {
                System.out.println("condition is met");
            }
            else {
                System.out.println("Number: " + i);
            }
            System.out.println("Next:");
        }
    }
}
